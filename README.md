# k3s_controlplane role

This role sets up a Kubernetes server (i.e. control plane).

It assumes there is an external mysql database already setup for permanent storage.

## Variables

* `k3s_controlplane_loadbalancer_ip`

    IP address of a load balancer that will forward requests to all
    control plane nodes.


* `k3s_controlplane_db_server`

    Address of MySQL/MariaDB database server. Port may be specified after colon.

    Example: "192.168.2.1:3306"

* `k3s_controlplane_db_user`

* `k3s_controlplane_db_password`

* `k3s_controlplane_db_name`
